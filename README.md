# xan's personal KiCad libraries

These are my personal KiCad libraries. They're not really meant for public
consumption.

3D models are excluded due to distribution limits; you can find them yourself
usually on digikey or snapeda.

## Schematic

- **xAnalog:** analog ICs
- **xConnector:** connectors
- **xDevice:** meant to replace KiCad's "Device". Some things are reformatted
  in my preferred style. US resistors and capacitors, pin 1 marks on symbols
  without pin numbers, etc.
- **xDiscretes:** individual part numbers of things in xDevice.
- **xDisplay:** displays (not including simple LEDs, etc)
- **xGraphics:** schematic-only symbols
- **xMixed:** mixed-signal (analog+digital) ICs
- **xPMIC:** power management ICs
- **xTubes:** vacuum tubes

## Footprint

The footprint libs are really a sloppy dumping ground. That said,

- **xDevice:** generally de facto "standard" devices. Some redos of footprints
  provided with KiCad.
- **xMisc:** even worse dumping ground. Generally manufacturer specific.

## License

Large chunks are pulled from the KiCad libraries, so [their license](https://gitlab.com/kicad/libraries/kicad-footprints/-/blob/master/LICENSE.md)
is used:

The KiCad libraries are licensed under the [Creative Commons CC-BY-SA 4.0
License](https://creativecommons.org/licenses/by-sa/4.0/legalcode), with the
following exception:

---------

_To the extent that the creation of electronic designs that use 'Licensed
Material' can be considered to be 'Adapted Material', then the copyright holder
waives article 3 of the license with respect to these designs and any generated
files which use data provided as part of the 'Licensed Material'._

---------

Of course they don't actually put a copyright claim in LICENSE.md so I'm not
sure how you're supposed to respect the "attribution" part of CC-BY-SA :/
